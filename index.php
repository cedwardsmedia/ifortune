<?php
    require("config.php");
    require("functions.php");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>BSD Fortune</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="terminal">
        <div class="output">
            <?php fortune(); ?>
        </div>
    </div>
</body>
</html>
